/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.iplocator.impl;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.timeZone;
import net.coalevo.iplocator.model.GeoLocation;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Class implementing {@link GeoLocation} based on an
 * encapsulated MaxMind GeoAPI <tt>Location</tt> instance.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GeoLocationImpl implements GeoLocation {

  private Location m_Location;
  private Locale m_Locale;

  public GeoLocationImpl(Location l) {
    m_Location = l;
  }//constructor

  public String getCountryName(Locale l) {
    return getLocale().getDisplayCountry(l);
  }//getCountryName

  public String getCountry() {
    return getLocale().getCountry();
  }//getCountry

  public String getISO3Country() {
    return getLocale().getISO3Country();
  }//getISO3Country

  public String getCity() {
    return m_Location.city;
  }//getCity

  public String getRegionName() {
    return m_Location.region;
  }//getRegionName

  public float getLongitude() {
    return m_Location.longitude;
  }//getLongitude

  public float getLatitude() {
    return m_Location.latitude;
  }//getLatitude

  public Locale getLocale() {
    if (m_Locale == null) {
      m_Locale = new Locale("", m_Location.countryCode);
    }
    return m_Locale;
  }//getLocale

  public TimeZone getTimeZone() {
    return TimeZone.getTimeZone(
        timeZone.timeZoneByCountryAndRegion(m_Location.countryCode, m_Location.region)
    );
  }//getTimeZone

}//class GeoLocationImpl
