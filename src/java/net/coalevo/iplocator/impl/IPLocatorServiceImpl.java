/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.iplocator.impl;

import com.maxmind.geoip.Location;
import net.coalevo.iplocator.service.IPLocatorService;
import net.coalevo.iplocator.model.GeoLocation;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.net.InetAddress;

/**
 * Class that implements {@link IPLocatorService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class IPLocatorServiceImpl
    implements IPLocatorService {

  private boolean m_Active = false;
  private IPLocatorStore m_DataStore;
  private Marker m_LogMarker = MarkerFactory.getMarker(IPLocatorServiceImpl.class.getName());

  public boolean activate() {
    //1. Check if active
    if (m_Active) {
      Activator.log().error(
          m_LogMarker,
          Activator.getBundleMessages().get("error.activate.active", "service", "UserdataService")
      );
      return false;
    }

    //2. prepare store
    m_DataStore = new IPLocatorStore();
    m_DataStore.activate();

    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    if (m_DataStore != null) {
      m_DataStore.deactivate();
    }
    m_DataStore = null;
    m_Active = false;
    return true;
  }//deactivate

  public GeoLocation getGeoLocation(InetAddress addr) {
    Location l = m_DataStore.lookup(addr);
    
    return new GeoLocationImpl(l);
  }//getGeoLocation

}//class IPLocatorServiceImpl
