/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.iplocator.impl;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.iplocator.service.IPLocatorConfiguration;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Provides the store for IP Geo-Location data.
 * <p/>
 * This implementation is based on the MaxMind GeoIP API and the
 * binary formatted data provided by MaxMind for Free (FOSS) or
 * for sale (visit http://www.maxmind.com).
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class IPLocatorStore
    implements ConfigurationUpdateHandler {

  private Messages m_BundleMessages;
  private LookupService m_LookupService;
  private Marker m_LogMarker = MarkerFactory.getMarker(IPLocatorStore.class.getName());

  public boolean activate() {
    m_BundleMessages = Activator.getBundleMessages();

    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    update(cm.getConfiguration());
    return true;
  }//activate

  public synchronized boolean deactivate() {
    m_BundleMessages = null;
    m_LookupService.close();
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {

    String dbfile = "";
    String cache = IPLocatorConfiguration.CACHE_INDEX;
    boolean update = true;

    try {
      dbfile = mtd.getString(IPLocatorConfiguration.DB_FILE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("IPLocatorStore.activation.configexception", "attribute", IPLocatorConfiguration.DB_FILE_KEY),
          ex
      );
    }

    //capture empty, translate to [working dir]/geoip/GeoLiteCity.dat
    if (dbfile.length() == 0) {
      dbfile = System.getProperty("user.dir") + File.separator + "geoip" + File.separator + "GeoLiteCity.dat";
    }

    try {
      cache = mtd.getString(IPLocatorConfiguration.CACHE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("IPLocatorStore.activation.configexception", "attribute", IPLocatorConfiguration.DB_FILE_KEY),
          ex
      );
    }
    try {
      update = mtd.getBoolean(IPLocatorConfiguration.DB_FILE_MONITOR);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("IPLocatorStore.activation.configexception", "attribute", IPLocatorConfiguration.DB_FILE_KEY),
          ex
      );
    }
    int params = -1;

    if (IPLocatorConfiguration.CACHE_INDEX.equals(cache)) {
      params = LookupService.GEOIP_INDEX_CACHE;
    } else if (IPLocatorConfiguration.CACHE_MEMORY.equals(cache)) {
      params = LookupService.GEOIP_MEMORY_CACHE;
    } else {
      params = LookupService.GEOIP_STANDARD;
    }

    if (update) {
      params = params | LookupService.GEOIP_CHECK_CACHE;
    }

    try {
      m_LookupService = new LookupService(
          dbfile,
          params
      );
    } catch (IOException ex) {
      Activator.log().error(m_LogMarker,"update",ex);
    }
  }//update

  public Location lookup(InetAddress addr) {
    return m_LookupService.getLocation(addr);
  }//lookup

}//class IPLocatorStore
