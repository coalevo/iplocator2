/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.iplocator.model;

import java.util.Locale;
import java.util.TimeZone;

/**
 * This interface defines a <tt>GeoLocation</tt> which provides
 * geographic location related information.
 * <p/>
 * Note that some of the provided information may not be available,
 * because it was not provided by the underlying database or the JRE.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface GeoLocation {

  /**
   * Returns the country display name in the given locale.
   *
   * @param l the <tt>Locale</tt> for which the country's display name
   *          should be returned.
   * @return the localized country's display name.
   */
  public String getCountryName(Locale l);

  /**
   * Returns the ISO two letter country code for this
   * <tt>GeoLocation</tt>.
   *
   * @return a <tt>String</tt> representing the two letter ISO
   *         code for the country.
   */
  public String getCountry();

  /**
   * Returns the ISO three letter country code for this
   * <tt>GeoLocation</tt>.
   *
   * @return a <tt>String</tt> representing the three letter ISO
   *         code for the country.
   */
  public String getISO3Country();

  /**
   * Returns the name of the city for this
   * <tt>GeoLocation</tt>.
   * <p/>
   * Note that the name will be returned in the default locale
   * stored in the underlying datasource.
   *
   * @return returns the city name.
   */
  public String getCity();

  /**
   * Returns the name of the region for this
   * <tt>GeoLocation</tt>.
   * <p/>
   * Note that the name will be returned in the default locale
   * stored in the underlying datasource.
   *
   * @return returns the region name.
   */
  public String getRegionName();

  /**
   * Returns the longitude for this <tt>GeoLocation</tt>.
   *
   * @return the longitude as <tt>float</tt>.
   */
  public float getLongitude();

  /**
   * Returns the latitude for this <tt>GeoLocation</tt>.
   *
   * @return the latitude as <tt>float</tt>.
   */
  public float getLatitude();

  /**
   * Returns a <tt>Locale</tt> for this <tt>GeoLocation</tt>.
   *
   * @return a <tt>Locale</tt> instance.
   */
  public Locale getLocale();

  /**
   * Returns the <tt>TimeZone</tt> for this <tt>GeoLocation</tt>.
   *
   * @return a <tt>TimeZone</tt> instance.
   */
  public TimeZone getTimeZone();

}//interface GeoLocation
