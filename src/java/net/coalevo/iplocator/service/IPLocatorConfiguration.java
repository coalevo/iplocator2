/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.iplocator.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface IPLocatorConfiguration {

  /**
   * Defines the key for the database file location configuration value.
   * This file should be a MaxMind GeoAPI compatible database file.
   */
  public static String DB_FILE_KEY = "database";

  /**
   * Defines the key for the flag that indicates whether the database
   * file should be monitored for changes, and the changes taken into account.
   */
  public static String DB_FILE_MONITOR = "lookup.filechange";

  /**
   * Defines if and which cache type should be used in the
   * lookup.
   */
  public static String CACHE_KEY = "lookup.cache";

  /**
   * Defines the cache type that will hold the complete database
   * in memory.
   * This type is not recommended for large databases, except there is
   * a lot of memory available (also to the JVM; make sure to adjust max heap -Xmx).
   */
  public static String CACHE_MEMORY = "memory";

  /**
   * Defines the cache type that will hold only the most frequently
   * accessed index portion of the database.
   * This type may be used for larger databases to speed up lookups, but
   * use less memory.
   * (Default)
   */
  public static String CACHE_INDEX = "index";

  /**
   * Defines that no cache will be used for lookups.
   * This type should be used if memory is a limited resource
   * and lookup speed is traded for low memory usage.
   */
  public static String CACHE_NONE = "none";


}//interface IPLocatorConfiguration
