/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.iplocator.service;

import net.coalevo.iplocator.model.GeoLocation;

import java.net.InetAddress;

/**
 * Provides the interface for the <tt>IPLocatorService</tt>.
 * <p/>
 * This service will provide geographic location information in form
 * of a {@link GeoLocation} instance; the included information will depend on the
 * underlying database.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface IPLocatorService {

  /**
   * Returns a {@link GeoLocation} instance for the given <tt>InetAddress</tt>.
   *
   * @param addr an <tt>InetAddress</tt>.
   * @return a {@link GeoLocation} instance.
   */
  public GeoLocation getGeoLocation(InetAddress addr);

}//interface IPLocatorService
